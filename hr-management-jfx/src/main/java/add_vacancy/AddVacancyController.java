package add_vacancy;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import vacancy.Vacancy;

public class AddVacancyController {
	@FXML
	private TextField vacName;

	@FXML
	private TextField numOfPosition;

	@FXML
	private TextField hiringMang;

	@FXML
	private TextField description;

	@FXML
	private Button save;

	@FXML
	private ComboBox<String> jobTitle;
	@FXML
	private Button admin;

	@FXML
	private Button aVback;

	public void aVback(ActionEvent event) {
		new Vacancy().show();
	}

	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

	public void save(ActionEvent event) throws SQLException {
		System.out.println(vacName.getText());
		System.out.println(numOfPosition.getText());
		System.out.println(hiringMang.getText());
		System.out.println(description.getText());
		System.out.println(jobTitle.getValue());

		if (vacName.getText().isEmpty() || numOfPosition.getText().isEmpty()|| hiringMang.getText().isEmpty()|| description.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from vacancy where vacancyname='" + vacName.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
			System.out.println("this Name is already present");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("name is already added");
			alert.showAndWait();
			return;

		} else {
			String query = "insert into vacancy( vacancyname,manager,jobTitle,vacDescription,numOfposition) "
					+ "values('" + vacName.getText() + "','" + hiringMang.getText() + "','" + jobTitle.getValue()
					+ "','" + description.getText() + "','" + numOfPosition.getText() + "')";

			DbUtil.executeQuery(query);
		}
	}
}