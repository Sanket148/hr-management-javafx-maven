package add_candidate;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import candidate.Candidates;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class AddCandidateController {
	@FXML
	private TextField candidateName;

	@FXML
	private Button aCBack;

	@FXML
	private TextField keyword;

	@FXML
	private Button candidateSave;

	@FXML
	private ComboBox<String> jobTitle;

	@FXML
	private ComboBox<String> hiring;

	@FXML
	private ComboBox<String> methOfApp;

	@FXML
	private ComboBox<String> status;

	@FXML
	private ComboBox<String> vacancy;

	@FXML
	private DatePicker dateOfAppTo;

	@FXML
	private DatePicker dateOfAppFrom;
	@FXML
	private Button admin;

	public void aCBack(ActionEvent event) {
		new Candidates().show();
	}

	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

	public void Candidatesave(ActionEvent event) throws SQLException {
		System.out.println(candidateName.getText());
		System.out.println(keyword.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(hiring.getValue());
		System.out.println(methOfApp.getValue());
		System.out.println(status.getValue());
		System.out.println(vacancy.getValue());
		System.out.println(dateOfAppTo.getValue());
		System.out.println(dateOfAppFrom.getValue());
		if (candidateName.getText().isEmpty() || keyword.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from candidate where name='" + candidateName.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
			System.out.println("this Name is already present");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("name is already added");
			alert.showAndWait();
			return;

		} else {

			String query = "insert into candidate( vacancy,name, status ,manager ,jobTitle, date_from ,date_to , keyword , meth_app ) values"
					+ "('" + vacancy.getValue() + "','" + candidateName.getText() + "','" + status.getValue() + "','"
					+ hiring.getValue() + "','" + jobTitle.getValue() + "','" + dateOfAppFrom.getValue() + "','"
					+ dateOfAppTo.getValue() + "','" + keyword.getText() + "','" + methOfApp.getValue() + "');";
			DbUtil.executeQuery(query);

		}

	}
}
