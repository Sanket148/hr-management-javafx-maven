package search_candidate;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import admin.HRAdmin;
import candidate.Candidates;
import db_Opration.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import search_user.User;

public class SearchCandidateController implements Initializable{
	
	@FXML
	private TextField SearchCandidate;

	@FXML
	private Button sCBack;
	@FXML
	private TableView<Candidate> tableView;
	@FXML
	private TableColumn<Candidate,String> col1;
	@FXML
	private TableColumn<Candidate,String> col2;
	@FXML
	private TableColumn<Candidate,String> col3;
	@FXML
	private TableColumn<Candidate,String> col4;
	
	@FXML
	private Button admin;

	private ObservableList<Candidate> data;


	
		public void sCBack(ActionEvent event) {
			new Candidates().show();
		}
		public void admin(ActionEvent event) {
			new HRAdmin().show();
		}

		@Override
		public void initialize(URL url, ResourceBundle rb) {

			data=FXCollections.observableArrayList();

			
			col1.setCellValueFactory(new PropertyValueFactory<Candidate, String>("firstName"));
			col2.setCellValueFactory(new PropertyValueFactory<Candidate, String>("vacancy"));
			col3.setCellValueFactory(new PropertyValueFactory<Candidate, String>("method"));

			col4.setCellValueFactory(new PropertyValueFactory<Candidate, String>("keyword"));
			

		buildData();
			
			FilteredList<Candidate> filteredData=new FilteredList<>(data,b->true);
			SearchCandidate.textProperty().addListener((observable,oldValue,newValue)->{
			filteredData.setPredicate(Candidate->{
				if(newValue.isEmpty() || newValue.isBlank() || newValue==null ) {
					return true;
				}
				String searchKeyword=newValue.toLowerCase();
				if(Candidate.getFirstName().toLowerCase().indexOf(searchKeyword)> -1) {
					return true;
				}else if(Candidate.getVacancy().toLowerCase().indexOf(searchKeyword)> -1) {
					return true;
				}else if(Candidate.getKeyword().toLowerCase().indexOf(searchKeyword)> -1) {
					return true;
				}else if(Candidate.getMethod().toLowerCase().indexOf(searchKeyword)> -1) {
					return true;
				}  {
					return false;
				}
					
				
			});
			});
			

			
			SortedList<Candidate> sortedData =new SortedList<>(filteredData);

			sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			tableView.setItems(sortedData);
			
			
		}
		public void buildData()
		{
			try {
				data=FXCollections.observableArrayList();
				String query = "Select*from candidate";
				System.out.println(query);
				ResultSet resultSet = DbUtil.executeQueryGetResult(query);
				while (resultSet.next()) {
					Candidate ca=new Candidate();
					ca.firstName.set(resultSet.getString(1));
					ca.vacancy.set(resultSet.getString(2));
					ca.keyword.set(resultSet.getString(3));
					ca.method.set(resultSet.getString(4));
				
					data.add(ca);
				}
				tableView.setItems(data);
				
			}catch(Exception ex) {
				ex.printStackTrace();
			}

		
		

		}

}
