package search_candidate;

import javafx.beans.property.SimpleStringProperty;

public class Candidate {
	public SimpleStringProperty firstName=new SimpleStringProperty();
	public SimpleStringProperty vacancy=new SimpleStringProperty();

	public SimpleStringProperty keyword=new SimpleStringProperty();
	public SimpleStringProperty method=new SimpleStringProperty();


	  public String getFirstName(){
	       return firstName.get();
	   }

	   

	   public String getVacancy(){
	       return vacancy.get();
	   }

	   public String getKeyword(){
	       return keyword.get();
	   }

	   public String getMethod(){
	       return method.get();
	   } 

}
