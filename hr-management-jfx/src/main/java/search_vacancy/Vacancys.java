package search_vacancy;

import javafx.beans.property.SimpleStringProperty;

public class Vacancys {
	public SimpleStringProperty jobtitle = new SimpleStringProperty();
	public SimpleStringProperty vacancy = new SimpleStringProperty();

	public SimpleStringProperty status = new SimpleStringProperty();

	public SimpleStringProperty manager = new SimpleStringProperty();
	//public SimpleStringProperty description = new SimpleStringProperty();

	public String getUserRole() {
		return jobtitle.get();
	}

	public String getEmployeeName() {
		return vacancy.get();
	}

	public String getStatus() {
		return status.get();
	}

	public String getManager(){
	       return manager.get();
	   }
	//public String getdescription(){
	    //   return description.get();
	 //  }
}
