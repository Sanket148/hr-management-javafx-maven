package search_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import admin.HRAdmin;
import db_Opration.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import search_user.SearchUser;
import search_user.User;
import vacancy.Vacancy;

public class SearchVacancyController implements Initializable {
	
	@FXML
	private Button dVSearch;
	
	@FXML
	private TextField SearchVac;
	
	@FXML
	private Button sVback;
	@FXML
	private Button admin;
	
	

	@FXML
	private TableView<Vacancys> tableView;
	@FXML
	private TableColumn<Vacancys,String> col1;
	@FXML
	private TableColumn<Vacancys,String> col2;
	@FXML
	private TableColumn<Vacancys,String> col3;
	@FXML
	private TableColumn<Vacancys,String> col4;
	@FXML
	private TableColumn<Vacancys,String> col5;
	
	private ObservableList<Vacancys> dataList=FXCollections.observableArrayList();
	
	private ObservableList<Vacancys> data;
	
	public void sVback(ActionEvent event) {
		new Vacancy().show();

		
	}
	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		data = FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<Vacancys, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<Vacancys, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<Vacancys, String>("status"));
		col4.setCellValueFactory(new PropertyValueFactory<Vacancys, String>("manager"));
		//col5.setCellValueFactory(new PropertyValueFactory<Vacancys, String>("description"));
		
		buildData();

		FilteredList<Vacancys> filteredData = new FilteredList<>(data, b -> true);
		SearchVac.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(Vacancys -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (Vacancys.getUserRole().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Vacancys.getEmployeeName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Vacancys.getStatus().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Vacancys.getManager().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				//} else if (Vacancys.getdescription().toLowerCase().indexOf(searchKeyword) > -1) {
					//return true;
				
				
				} else 
					return false;

			});
		});

		SortedList<Vacancys> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from vacancy";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Vacancys users = new Vacancys();
				users.jobtitle.set(resultSet.getString(2));
				users.vacancy.set(resultSet.getString(3));
				users.status.set(resultSet.getString(4));
				users.manager.set(resultSet.getString(5));
			//	users.description.set(resultSet.getString(5));


				data.add(users);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
