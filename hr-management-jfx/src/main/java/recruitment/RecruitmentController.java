package recruitment;

import admin.HRAdmin;
import candidate.Candidates;
import dashboard.HRDashboard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import vacancy.Vacancy;

public class RecruitmentController {
@FXML
private Button candidate;

@FXML
private Button vacancy;

@FXML
private Button dashBack;
@FXML
private Button admin;

public void candidate(ActionEvent event) {
	new Candidates().show();
}
public void vacancy(ActionEvent event) {
	new Vacancy().show();
}

public void dashBack(ActionEvent event) {
	new HRDashboard().show();
}
public void admin(ActionEvent event) {
	new HRAdmin().show();
}



}
