package delete_vacancy;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import vacancy.Vacancy;

public class DeleteVacancyController {
	@FXML
	private Button dVback;
	
	@FXML
	private Button dVDelete;
	
	@FXML
	private TextField deleteVac;
	@FXML
	private Button admin;
	
	

	public void dVback(ActionEvent event) {
		new Vacancy().show();
	}
	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

	
	public void dVDelete(ActionEvent event) throws SQLException {
		System.out.println(deleteVac.getText());
		if (deleteVac.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from vacancy where vacancyname='" + deleteVac.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
		String query="delete from vacancy where vacancyname='"+deleteVac.getText()+"'";
		
		DbUtil.executeQuery(query);
		new Vacancy().show();

		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Check vacancy name to Delete");
			alert.showAndWait();
			return;
		}
	
	}
}
