package edit_vacancy;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import vacancy.Vacancy;

public class EditVacancyController {

	@FXML
	private TextField editVac;

	@FXML
	private TextField vacName;

	@FXML
	private TextField numOfPosition;

	@FXML
	private TextField hiringMang;

	@FXML
	private TextField description;

	@FXML
	private Button save;

	@FXML
	private ComboBox<String> jobTitle;

	@FXML
	private Button eVback;
	@FXML
	private Button admin;

	public void eVback(ActionEvent event) {
		new Vacancy().show();
	}

	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

	public void save(ActionEvent event) throws SQLException {
		System.out.println(vacName.getText());
		System.out.println(numOfPosition.getText());
		System.out.println(hiringMang.getText());
		System.out.println(description.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(editVac.getText());
		
		if (editVac.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from vacancy where vacancyname='" + editVac.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
		


			String query = "update vacancy set jobtitle ='" + jobTitle.getValue() + "',vacancyname" + " ='"
					+ vacName.getText() + "',manager ='" + hiringMang.getText() + "',vacDescription ='"
					+ description.getText() + "' where vacancyname='" + editVac.getText() + "';";

			DbUtil.executeQuery(query);
			new Vacancy().show();

			
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Check vacancy name to edit");
			alert.showAndWait();
			return;
		}


	}
}
