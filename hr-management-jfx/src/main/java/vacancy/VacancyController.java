package vacancy;

import add_vacancy.AddVacancy;
import admin.HRAdmin;
import delete_vacancy.DeleteVacancy;
import edit_vacancy.EditVacancy;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recruitment.Recruitment;
import search_vacancy.SearchVacancy;

public class VacancyController {

	@FXML
	private Button add;

	@FXML
	private Button edit;

	@FXML
	private Button search;

	@FXML
	private Button delete;
	
	@FXML
	private Button vBack;
	@FXML
	private Button admin;

	public void add(ActionEvent event) {
		new AddVacancy().show();
	}

	public void edit(ActionEvent event) {
		new EditVacancy().show();

	}

	public void search(ActionEvent event) {
		new SearchVacancy().show();
	}

	public void delete(ActionEvent event) {
		new DeleteVacancy().show();
	}
	
	public void vBack(ActionEvent event) {
		new Recruitment().show();

	}
	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}


}