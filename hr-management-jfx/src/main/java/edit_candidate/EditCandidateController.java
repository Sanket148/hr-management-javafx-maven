package edit_candidate;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import candidate.Candidates;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import vacancy.Vacancy;

public class EditCandidateController {
	@FXML
	private Button eCBack;

	@FXML
	private TextField editCandidate;

	@FXML
	private TextField candidateName;

	@FXML
	private TextField keyword;

	@FXML
	private Button candidateSave;

	@FXML
	private ComboBox<String> jobTitle;

	@FXML
	private ComboBox<String> hiring;

	@FXML
	private ComboBox<String> methOfApp;

	@FXML
	private ComboBox<String> status;

	@FXML
	private ComboBox<String> vacancy;

	@FXML
	private DatePicker dateOfAppTo;

	@FXML
	private DatePicker dateOfAppFrom;
	@FXML
	private Button admin;

	public void eCBack(ActionEvent event) {
		new Candidates().show();
	}

	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

	public void candidateSave(ActionEvent event) throws SQLException {
		System.out.println(editCandidate.getText());
		System.out.println(candidateName.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(hiring.getValue());
		System.out.println(methOfApp.getValue());
		System.out.println(status.getValue());
		System.out.println(vacancy.getValue());
		System.out.println(dateOfAppTo.getValue());
		System.out.println(dateOfAppFrom.getValue());

		if (editCandidate.getText().isEmpty() || candidateName.getText().isEmpty() || hiring.getValue().isEmpty()
				|| keyword.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from candidate where name='" + editCandidate.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {

			String query = "update candidate set name ='" + candidateName.getText() + "',status" + " ='"
					+ status.getValue() + "',manager ='" + hiring.getValue() + "',jobTitle ='" + jobTitle.getValue()
					+ "',date_from ='" + dateOfAppFrom.getValue() + "',date_to ='" + dateOfAppTo.getValue()
					+ "',keyword ='" + keyword.getText() + "',meth_app ='" + methOfApp.getValue() + "' where name='"
					+ editCandidate.getText() + "';";
			DbUtil.executeQuery(query);
			new Candidates().show();


		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Check candidate name to edit");
			alert.showAndWait();
			return;
		}

	}
}
