package search_user;


import javafx.beans.property.SimpleStringProperty;

public class User {
	public SimpleStringProperty userRole=new SimpleStringProperty();
	public SimpleStringProperty employeeName=new SimpleStringProperty();
	public SimpleStringProperty status=new SimpleStringProperty();
	public SimpleStringProperty userName=new SimpleStringProperty();
	public SimpleStringProperty password=new SimpleStringProperty();
	public SimpleStringProperty confirmPassword=new SimpleStringProperty();
	

	  public String getUserRole(){
	       return userRole.get();
	   }

	   public String getEmployeeName(){
	       return employeeName.get();
	   }

	   public String getStatus(){
	       return status.get();
	   }

	   public String getUserName(){
	       return userName.get();
	   }

	   public String getPassword(){
	       return password.get();
	   } public String getConfirmPassword(){
	       return confirmPassword.get();
	   }

}