package candidate;

import add_candidate.AddCandidate;
import admin.HRAdmin;
import delete_candidate.DeleteCandidate;
import edit_candidate.EditCandidate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recruitment.Recruitment;
import search_candidate.SearchCandidate;

public class CandidateController {

	@FXML
	private Button add;

	@FXML
	private Button edit;

	@FXML
	private Button search;

	@FXML
	private Button delete;
	
	@FXML
	private Button recBack;
	
	@FXML
	private Button admin;

	public void add(ActionEvent event) {
		new AddCandidate().show();
	}

	public void edit(ActionEvent event) {
		new EditCandidate().show();
	}

	public void search(ActionEvent event) {
		new SearchCandidate().show();
	}

	public void delete(ActionEvent event) {
		new DeleteCandidate().show();
	}
	
	public void recBack(ActionEvent event) {
		new Recruitment().show();
	}
	public void admin(ActionEvent event) {
		new HRAdmin().show();
	}

}
