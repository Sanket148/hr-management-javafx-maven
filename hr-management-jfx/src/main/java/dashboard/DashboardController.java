package dashboard;

import admin.HRAdmin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recruitment.Recruitment;

public class DashboardController {

	@FXML
	private Button admin;
	
	@FXML
	private Button recruitment;
	
	public void admin(ActionEvent event) {
		new HRAdmin().show();
		
	}
	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
}
