package add_user;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class AddUserController {

	@FXML
	private Button aUBack;

	@FXML
	private TextField empName;

	@FXML
	private TextField password;

	@FXML
	private TextField confPassword;

	@FXML
	private TextField userName;

	@FXML
	private ComboBox<String> user;

	@FXML
	private ComboBox<String> status;

	@FXML
	private Button save;

	@FXML
	private Button recruitment;

	public void aUback(ActionEvent event) {
		new HRAdmin().show();
	}

	public void aUAdmin(ActionEvent event) {

		new HRAdmin().show();

	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}

	public void save(ActionEvent event) throws SQLException {
		System.out.println(empName.getText());
		System.out.println(password.getText());
		System.out.println(confPassword.getText());
		System.out.println(userName.getText());
		System.out.println(user.getValue());
		System.out.println(status.getValue());
		if (empName.getText().isEmpty() || userName.getText().isEmpty() || confPassword.getText().isEmpty()
				|| password.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from user where userName='" + userName.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
			System.out.println("this userName is already present");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("userName is already added");
			alert.showAndWait();
			return;

		} else {
			if (password.getText().equals(confPassword.getText())) {
				String query = "insert into user( username,password,confirmPassword,userRole, employeename, status1) values('"
						+ userName.getText() + "','" + password.getText() + "','" + confPassword.getText() + "','"
						+ user.getValue() + "','" + empName.getText() + "','" + status.getValue() + "');";
				DbUtil.executeQuery(query);

			} else {
				Alert alert = new Alert(Alert.AlertType.WARNING);
				alert.setHeaderText("Warning!");
				alert.setTitle("Warning");
				alert.setContentText("Password Does Not Macthed");
				alert.showAndWait();
			}

		}

	}
}
