package delete_candidate;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import candidate.Candidates;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class DeleteCandidateController {
	@FXML
	private Button dCBack;
	
	@FXML
	private TextField deleteCandidate;
	@FXML
	private Button admin;
	
	
	@FXML
	private Button delete;
		public void dCBack(ActionEvent event) {
			new Candidates().show();
		}
		public void admin(ActionEvent event) {
			new HRAdmin().show();
		}

	public void delete(ActionEvent event) throws SQLException {

		
		System.out.println(deleteCandidate.getText());
		if (deleteCandidate.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from candidate where name='" + deleteCandidate.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
		String query="delete from candidate where name='"+deleteCandidate.getText()+"'";
		
		DbUtil.executeQuery(query);
		new Candidates().show();

		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Check candidate name to Delete");
			alert.showAndWait();
			return;
		}
	}
}
