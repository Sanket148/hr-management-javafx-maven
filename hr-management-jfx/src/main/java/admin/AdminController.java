package admin;

import add_user.AddUser;
import dashboard.HRDashboard;
import delete_user.DeleteUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recruitment.Recruitment;
import search_user.SearchUser;

public class AdminController {

	@FXML
	private Button addUser;
	
	@FXML
	private Button searchUser;
	
	@FXML
	private Button deleteUser;
	
	@FXML
	private Button dashBack;
	
	@FXML
	private Button recruitment;
	
	
	
	public void addUser(ActionEvent event) {
		new AddUser().show();
	}
	
	public void searchUser(ActionEvent event) {
		new SearchUser().show();
	}
	
	public void deleteUser(ActionEvent event) {
		new DeleteUser().show();
		
	}
	
	public void dashBack(ActionEvent event) {
		new HRDashboard().show();
	}
	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
}

