package delete_user;

import java.sql.ResultSet;
import java.sql.SQLException;

import admin.HRAdmin;
import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class DeleteUserController {
	
	@FXML
	private Button dUDelete;
	
	@FXML
	private TextField deleteUser;
	
	
	@FXML
	private Button dUback;
	

	@FXML
	private Button recruitment;
	
	
	
	public void dUback(ActionEvent event) {
		new HRAdmin().show();
	}
	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dUDelete(ActionEvent event) throws SQLException {
		System.out.println(deleteUser.getText());
		if (deleteUser.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		String query1 = "select count(*) from user where employeeName='" + deleteUser.getText() + "';";
		ResultSet rs1 = DbUtil.executeQueryGetResult(query1);
		rs1.next();
		if (rs1.getInt(1) == 1) {
		String query="delete from user where employeeName='"+deleteUser.getText()+"'";
		
		DbUtil.executeQuery(query);
		new HRAdmin().show();

		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Check user name to Delete");
			alert.showAndWait();
			return;
		}
	}
}
